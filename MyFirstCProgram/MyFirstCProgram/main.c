//
//  main.c
//  MyFirstCProgram
//
//  Created by turner on 9/26/13.
//  Copyright (c) 2013 treehouse. All rights reserved.
//

#include <stdio.h>
//#include <Cocoa/Cocoa.h> //needed for NSArray
//#include "MyCircle.h" //quotes as referencing workspace file. this is where the struct declarations go.

//declare funcs:
int addNumbers(int a, int b);
int xLargerThanY(int x, int y);
void switchChar(char l);

// see MyCircle.m and .h

//declare struct:
typedef struct {
    //declare properties:
    float centre[3];
    float radius;
} MyCircle;
MyCircle makeSphere(float *c, float r); //why pointer for array but not for float??

int main()
{

    // insert code here...
    printf("Hello, World!\n");
    int a = 1;
    int b = 2;
    a += b;
    printf("Adding b %d to a %d \n", b, a);
    int c = addNumbers(a,b);
    printf ("Adding up: a %d + b %d = c %d \n",a,b,c);
    
    char letter = 'c';
    switchChar(letter);
    
    printf ("\n");
    float numbers[] = {0.0, 1.0, 3.14159};
    int len = (sizeof(numbers) / sizeof(float));//array length
    printf ("Memory size in bytes %ld for array, %ld float, %d items \n",sizeof(numbers), sizeof(float), len);
    int i = 0;
    while (i < len) {
        printf("Floating array number %d is %f \n",i,numbers[i]);
        i++;
    }
    char *letterPointer; //Declare an uninitialized char pointer variable named "pointy".
    letterPointer = &letter; //Assign the variable "pointy" the memory address of "alpha".
    
    printf("letter %c is always the same as *letterPointer %c \n", letter, *letterPointer);
    
    letter = 'd';
    printf("letter %c is always the same as *letterPointer %c \n", letter, *letterPointer);
    
    letter = 'a';
    printf("letter %c is always the same as *letterPointer %c \n", letter, *letterPointer);

    char helloworld[] = "helloworld";
    letterPointer = &helloworld[3];
    printf("*letterPointer %c is in helloworld[] %s \n", *letterPointer, helloworld);
    
    letterPointer++;
    printf("*letterPointer %c is in helloworld[] %s \n", *letterPointer, helloworld);
    ++letterPointer;
    printf("*letterPointer %c is in helloworld[] %s \n", *letterPointer, helloworld);
    
    int myNumber;
    int *memAddressPointer = &myNumber;
    //memAddressPointer contains the memory address of myNumber
    printf("*memAddressPointer %d, &myNumber %d, myNumber %d, memAddressPointer %d \n",*memAddressPointer, &myNumber,myNumber,memAddressPointer); //complains as memaddress pointer and &mynumber should be pointers.
    //*memAddressPointer = myNumber
    //&myNumber = memAddressPointer
    
    float centrePoint[] = { 20, 30, 40 }; //x,y,z
    //[NSArray arrayWithObjects:aDate, aValue, aString, nil]
    //NSArray *centrePoint = @[ 20, 30, 40 ];
    float myRadius = 10;
    
    // see MyCircle.h and .m
    MyCircle ball = makeSphere(centrePoint, myRadius);
    //MyCircle *ball = [[MyCircle alloc] init]; //makeSphere(centrePoint, myRadius);
    //[ball setRadius:myRadius];
    
    
    float x=ball.centre[0];float y=ball.centre[1];float z=ball.centre[2];float r=ball.radius;
    printf(" making a ball - struct MyCircle - func makeSphere - centre: %f %f %f, radius: %f ",x,y,z,r);
    
    return 0;
}
void myFunction() {
    printf("myFunction \n");
}
int addNumbers(int a, int b) {
    printf("addNumbers %d, %d \n", a, b);
    return a+b;
}
int xLargerThanY(int x, int y) {
    printf("xLargerThanY %d, %d \n", x, y);
    int yes = 0;
    if (x>y) yes = 1;
    return yes;
}
void compareNum(int x) {
    printf("compareNum %d ", x);
    if (x==0) {
        printf("zero");
    } else if (x>0) {
        printf("positive ");
    } else if (x<0) {
        printf("negative");
    }
    printf ("\n");

}
void switchChar(char l){
    printf("switchChar %c ", l);
    
    switch (l) {
        case 'a':
            printf("switchChar a");
            break;
        case 'b':
            printf("switchChar b");
            break;
        case 'c':
            printf("switchChar c");
            break;
        default:
            printf("Not equal to a,b,c");
            break;
    }
    printf ("\n");

}

//structs - typedef at top, creation function - set vars and return instance
MyCircle makeSphere(float *c, float r) {
    MyCircle round; //struct name, instance name
    
    printf("makeSphere c %f %f %f r %f \n",c[0],c[1],c[2],r);
    
    round.centre[0] = c[0];
    round.centre[1] = c[1];
    round.centre[2] = c[2];
    round.radius = r;
    
    return round;
}



