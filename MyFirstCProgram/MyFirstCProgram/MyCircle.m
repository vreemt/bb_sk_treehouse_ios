//
//  MyCircle.m
//  MyFirstCProgram
//
//  Created by Sandra Koning on 27/06/2014.
//  Copyright (c) 2014 treehouse. All rights reserved.
//
// main file
//  implementation - internal variable names
//  methods and getters/setters for variables

#import "MyCircle.h"

@implementation MyCircle
    
-(void)setRadius:(float)r  {
    printf("MyCircle setRadius radius was %f will be %f \n",_radius,r);
    _radius = r;
}
-(float)r {
    printf("MyCircle r - get radius %f \n",_radius);
    return _radius;
}
-(void)setCentre:(NSArray *)c {
    printf("MyCircle setCentre was %f %f %f \n",[[_centre objectAtIndex: 0] floatValue],[[_centre objectAtIndex: 0] floatValue],[[_centre objectAtIndex: 0] floatValue]);
    
    
    _centre = c;

    
    //Set values of centreXYZ
    _centreX = [[_centre objectAtIndex: 0] floatValue];
    _centreY = [[_centre objectAtIndex: 1] floatValue];
    _centreZ = [[_centre objectAtIndex: 2] floatValue];
    
    printf("MyCircle setCentre now %f %f %f \n", _centreX,_centreY,_centreZ);
}
-(NSArray *)c {
    printf("MyCircle c - get centre \n"); //%f %f %f \n",_centre[0],_centre[1],_centre[2]);
    return _centre;
}
-(void)setCentreXYZ:(float)cX y:(float)cY z:(float)cZ {
    //create NSArray with floats
    printf("MyCircle setCentreXYZ was %f %f %f will be %f %f %f   \n",_centreX,_centreY,_centreZ,cX,cY,cZ);
    
    NSArray *newCentre = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:cX],
                            [NSNumber numberWithFloat:cY],
                            [NSNumber numberWithFloat:cZ],nil];
    //[setCentre newCentre];
    [self setCentre:newCentre];
    
    _centreX = cX;
    _centreY = cY;
    _centreX = cZ;
}
-(float)cX {
    printf("MyCircle cX - get centreX %f \n",_centreX);
    return _centreX;
}
-(float)cY {
    printf("MyCircle cY - get centreY %f \n",_centreY);
    return _centreY;
}
-(float)cZ {
    printf("MyCircle cZ - get centreZ %f \n",_centreZ);
    return _centreZ;
}
@end
