//
//  MyCircle.h
//  MyFirstCProgram
//
//  Created by Sandra Koning on 27/06/2014.
//  Copyright (c) 2014 treehouse. All rights reserved.
//
// header file
//  interface - internal variable names
//  declaration of functions and variables

#import <Foundation/Foundation.h>

@interface MyCircle : NSObject {
    NSArray *_centre;
    float _centreX;
    float _centreY;
    float _centreZ;
    
    float _radius;
}
-(void)setRadius:(float)r;
-(float)r;
-(void)setCentre:(NSArray *)c;
-(NSArray *)c;
-(void)setCentreXYZ:(float)cX y:(float)cY z:(float)cZ;
-(float)cX;
-(float)cY;
-(float)cZ;


@end
